package com.example.assignment1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import org.mariuszgromada.math.mxparser.Expression

class MainActivity : AppCompatActivity() , View.OnClickListener{


    val TAG : String = "MainActivity"
    private var breackets : Int =0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        initSetup()
    }

    private fun initSetup(){



        // Consuming Touch Event For Force to not open Keyboard
        edittext.setOnTouchListener(object : View.OnTouchListener{
            override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                return true
            }

        })


        button.setOnClickListener(this)
        button9.setOnClickListener(this)
        button10.setOnClickListener(this)
        button11.setOnClickListener(this)
        button12.setOnClickListener(this)
        button13.setOnClickListener(this)
        button14.setOnClickListener(this)
        button15.setOnClickListener(this)
        button16.setOnClickListener(this)
        button17.setOnClickListener(this)
        button18.setOnClickListener(this)
        button19.setOnClickListener(this)
        button20.setOnClickListener(this)
        button21.setOnClickListener(this)
        button22.setOnClickListener(this)
        button23.setOnClickListener(this)
        button28.setOnClickListener(this)
        button29.setOnClickListener(this)
        button30.setOnClickListener(this)
        button31.setOnClickListener(this)






    }

    override fun onClick(p0: View?) {
        //Log.d(TAG,"Clickedddd")
        when (p0?.id){
            button12.id -> clearFields()
            button10.id -> backspace()
            button23.id -> resultExp()
            button20.id -> manageBrackets()
            else -> addText(p0?.id)
        }
    }

    private fun addText(id : Int?) {
        var button1=findViewById<View>(id!!) as Button
        edittext.append(button1.text.toString())

        updateResult()
    }

    private fun updateResult() {
        var currentExp = edittext.text.toString()
        var expression : Expression = Expression(currentExp)
        var res=expression.calculate()
        if (res.isNaN()) {
            result.setText("")
            return
        }
        result.setText(expression.calculate().toString())
    }

    private fun manageBrackets() {
        var currentexp=edittext.text.toString()
        var current_pos=currentexp.length-1
        if (current_pos<0) {
            breackets++
            edittext.append("(")
        }
        else{
            if (currentexp[current_pos] == '('){
                breackets++
                edittext.append("(")
            }
            else{

                if(breackets >= 1) {
                    edittext.append(")")
                    breackets--
                }
                else{
                    breackets++
                    edittext.append("(")
                }
            }
        }

        updateResult()


    }

    private fun resultExp() {
        var currentExp = edittext.text.toString()
        var expression : Expression = Expression(currentExp)
        var res=expression.calculate()
        if (res.isNaN()) return
        edittext.setText(expression.calculate().toString())

    }

    private fun backspace() {
        var currentexp=edittext.text.toString()
        if (currentexp.length == 0) return
        if (currentexp[currentexp.length-1] == '(') breackets--
        if (currentexp[currentexp.length-1] == ')') breackets++

        currentexp=currentexp.substring(0,currentexp.length-1)
        edittext.setText(currentexp)

        updateResult()

    }

    private fun clearFields(){
        edittext.setText("")
        result.setText("")
        breackets=0
    }


}