package com.example.assignment1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatDelegate
import com.google.android.material.animation.AnimationUtils


class SplashScreen : AppCompatActivity() {
    lateinit var textview2: TextView
    lateinit var textview3: TextView
    lateinit var textview4: TextView
    lateinit var textview5: TextView
    lateinit var textview6: TextView
    lateinit var textview7: TextView
    lateinit var textview8: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_splash_screen)
        supportActionBar?.hide()
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN)
        init()
    }
    
    private fun init(){
        textview2=findViewById(R.id.textView2)
        textview3=findViewById(R.id.textView3)
        textview4=findViewById(R.id.textView4)
        textview5=findViewById(R.id.textView5)
        textview6=findViewById(R.id.textView6)
        textview7=findViewById(R.id.textView7)
        textview8=findViewById(R.id.textView8)


        textview2.startAnimation(android.view.animation.AnimationUtils.loadAnimation(applicationContext,R.anim.anim_2))
        textview3.startAnimation(android.view.animation.AnimationUtils.loadAnimation(applicationContext,R.anim.anim_3))
        textview4.startAnimation(android.view.animation.AnimationUtils.loadAnimation(applicationContext,R.anim.anim_4))
        textview5.startAnimation(android.view.animation.AnimationUtils.loadAnimation(applicationContext,R.anim.anim_5))
        textview6.startAnimation(android.view.animation.AnimationUtils.loadAnimation(applicationContext,R.anim.anim_6))
        textview7.startAnimation(android.view.animation.AnimationUtils.loadAnimation(applicationContext,R.anim.anim_7))
        textview8.startAnimation(android.view.animation.AnimationUtils.loadAnimation(applicationContext,R.anim.anim_8))


        Handler().postDelayed(object : Runnable{
            override fun run() {
                startActivity(Intent(this@SplashScreen,MainActivity::class.java))
                finish()
            }

        },10000)

    }
}